import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

import Alert from '../component/Alert';
import AmountWallet from '../component/AmountWallet';
import InputWallet from '../component/InputWallet';
import SelectCoin from "../component/SelectCoin";
import Spinner from "../component/Spinner";

import { SERVER } from "../config";

interface Props {
    name: string
}  
class AddWallet extends React.Component<Props> {

    state = {
      wallet: {
        status: 0,
        address: '',
        balance: 0,
        price_eur: 0,
        price_usd: 0,
        days: 0
      },
      showSpinner: false,
      status: 0
    };

    async handleOnChangeInput(params:any) {
      if(params) {        
        this.setState({showSpinner: true});

        const response = await axios.post(`${SERVER}/api/wallet`, params);

        if(response.status === 201)
          this.setState({wallet: response.data});

        this.setState({showSpinner: false});
      } else {
        this.setState({wallet: { status: 1 }});
      }
    }

    getCardPrice() {
      const { price_eur, price_usd } = this.state.wallet;

      return (
        <div className="col">
          <SelectCoin 
            price_usd={price_usd} 
            price_eur={price_eur} 
          />
        </div>
      )
    }

    getCardWallet() {
      const { wallet } = this.state;
      console.log('status', wallet.status);

      if(wallet.status === 1) {
        return (
          <div className="col">
            <AmountWallet 
              onPress={() => this.setState({wallet: {status: 0}})} 
              balance={wallet.balance} 
              address={wallet.address} 
            />
          </div>
        )
      }

      return (
        <div className="col">
          <InputWallet onChange={this.handleOnChangeInput.bind(this)} />
        </div>
      )
    }

    render() {
      const { wallet, showSpinner } = this.state;
      return (
          <Fragment>
            <h2> 
              <Link to="/" className="btn btn-lg link-light">
                <FontAwesomeIcon icon={faAngleLeft} color="gray" />
              </Link>
              Add wallet <Spinner show={showSpinner} />
            </h2>
            <p>Enter your wallet for more info</p>

            <Alert days={wallet.days} />

            <div className="row">
              {this.getCardWallet()}
              {this.getCardPrice()}
            </div>
          </Fragment>
      );
    }
  }

  export default AddWallet;