import React, { Fragment } from "react";
import { Link } from 'react-router-dom';
import axios from "axios";

import * as _ from "lodash";

import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';

import Button from '../component/Button';
import CheckBox from "../component/CheckBox";
import Spinner from "../component/Spinner";
import StatusWallet from "../component/StatusWallet";

import { SERVER } from '../config';

interface Props {

}
interface State {
    items: any[],
    showSpinner: boolean,
    orderAsc: boolean,
    arrow: any,
    checkedItems: any,
}
class ListWallet extends React.Component<Props, State> {
    
    state = {
        items: [],
        showSpinner: true,
        orderAsc: false,
        arrow: faArrowDown,
        checkedItems: new Map(),
    };

    componentDidMount() {
        fetch(`${SERVER}/api/wallet`)
        .then((res) => res.json())
        .then((json) => {
            this.setState({
                items: _.orderBy(json, ["favourite"], ["desc"])
            });
            json.map((item:any) => {
                this.setState(prevState => ({ 
                    checkedItems: prevState.checkedItems.set(item._id, item.favourite) 
                }));
            })
        })
        .finally(() => {
            this.setState({
                showSpinner: false
            });
        })
    }

    async handleChange(e:any) {
        const item = e.target.id;
        const isChecked = e.target.checked;

        const params = {
            favourite: isChecked
        }

        await axios.put(`${SERVER}/api/wallet/${item}`, params);
        
        this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item, isChecked) }));
    }

    handleOrder() {
        const { items, orderAsc } = this.state;
        
        let orderBy = [];
        let arrow;

        if(!orderAsc) {
            orderBy = _.orderBy(items, ["favourite"], ["asc"]);
            arrow = faArrowUp;
         } else {
            orderBy = _.orderBy(items, ["favourite"], ["desc"]);
            arrow = faArrowDown;
         }

         this.setState({items: orderBy, orderAsc: !orderAsc, arrow: arrow});
    }
  
    render() {
        const { showSpinner, arrow } = this.state;
      return (
        <Fragment>
            <h2>
                Wallets <Spinner show={showSpinner} />
            </h2>
            <p>List of tested wallets</p>
            <div className="card bg-blue-100">
                <div className="card-header">
                    <div className="d-flex justify-content-end mb-2">
                        <Link to="wallets/add">Add wallet</Link>
                    </div>
                </div>
                <div className="card-body">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">
                                    Favourite <Button icon={arrow} color={'#ccc'} onPress={this.handleOrder.bind(this)} />
                                </th>
                                <th scope="col">Address</th>
                                <th scope="col">Balance</th>
                                <th scope="col">EUR</th>
                                <th scope="col">USD</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.getWallets()}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <h4 className="mt-5">Requirements</h4>
                    <ol>
                        <li>
                            Add wallet addresses and display them.
                        </li>
                        <li>
                            From the set of wallets, the user should be able to select favorites and order by them.
                        </li>
                        <li>
                            We should have a way to know if a wallet is old. A wallet is considered old if the first transaction was performed at least one year ago.
                        </li>                        
                        <li>
                            The user should be able to do the following actions:
                            <ol>
                                <li>
                                    Get exchange rates from Euro and US Dollar to ETH (Ethereum), those can be stored in-memory or in any DB of your preference.
                                </li>
                                <li>
                                    Edit the exchange rate of Euro or US Dollar to ETH.
                                </li>

                            </ol>
                        </li>
                        <li>
                            Given a currency (Euro or US Dollar) then the user should have the balance of the ETH
        in the wallet in the selected currency using the exchange rates from step 4.
                        </li>
                    </ol>
                </div>
            </div>
        </Fragment>
      );
    }

    private getWallets() {
        const { showSpinner, items } = this.state;
        if (showSpinner) {
            return <tr><td colSpan={7} align="center">Please wait some time... </td></tr>;
        }

        if (!showSpinner && items.length < 1) {
            return (
                <tr>
                    <td colSpan={7} align="center">
                        <Link to="wallets/add" className="btn btn-outline-info">Add wallet</Link>
                    </td>
                </tr>
            );
        }

        return (
            <Fragment>
            {
                items.map((item:any) => {
                    const color = item.days > 365 ? "table-danger" : "";
                    const { checkedItems } = this.state;
                    return (
                        <tr key={item._id} className={color}>
                            <th scope="row" style={{width: 100}}>{item._id}</th>
                            <td align="center">
                                <CheckBox 
                                    id={item._id} 
                                    checked={checkedItems.get(item._id)} 
                                    onChange={this.handleChange.bind(this)} 
                                />
                            </td>
                            <td>{item.address}</td>
                            <td>{item.balance}</td>
                            <td>{item.price_eur}</td>
                            <td>{item.price_usd}</td>
                            <td>
                                <StatusWallet days={item.days} />                            
                            </td>
                        </tr>    
                    )}
                )
            }
            </Fragment>
        )
    }
  }

  export default ListWallet;