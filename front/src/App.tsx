import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.css';

import AddWallet from './screen/AddWallet'
import ListWallet from './screen/ListWallet';

function App() {
  return (
    <BrowserRouter>
      <div className="container mt-5">
        <div className="row">
          <div className="col align-self-center">
            <Routes>
              <Route path="/" element={<ListWallet />} />
              <Route path="wallets/add" element={<AddWallet name="Diego" />} />
            </Routes>
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App