import React, { Fragment } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';

interface Props {
    id: string,
    checked: boolean,
    onChange: any
}
  
class CheckBox extends React.Component<Props> {


    render() {
        const { id, checked, onChange } = this.props
      return (
        <Fragment key={id}>
            <input type="checkbox" 
                className="btn-check" 
                id={`${id}`} 
                checked={checked} 
                onChange={onChange}
            />
            <label className="btn btn-link-light btn-sm p-0" htmlFor={`${id}`}>
                <FontAwesomeIcon icon={faStar} color={checked ? '#ffc107': '#ccc'} />
            </label>
        </Fragment>
);
    }
  }

  export default CheckBox;