import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faWarning } from '@fortawesome/free-solid-svg-icons';

interface Props {
    days: number
}
  
class StatusWallet extends React.Component<Props> {

    render() {
        let icon = <FontAwesomeIcon icon={faCheck} color="green" title="Wallet is ok!" />;

        if(this.props.days > 365) {
            icon = <FontAwesomeIcon icon={faWarning} color="#842029" title="Wallet is old!" />   
        }

      return icon;
    }
  }

  export default StatusWallet;