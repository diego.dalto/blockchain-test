import React from 'react';

import { faEdit } from '@fortawesome/free-solid-svg-icons';

import Button from "./Button";

interface Props {    
    onPress: any,
    address: string,
    balance: number
}  

class AmountWallet extends React.Component<Props> {

    render() {
      return (
        <div className="card bg-info bg-opacity-10">
            <div className="card-body min-vh-25">
                <div className="d-flex justify-content-end">
                    <Button icon={faEdit} color="blue" onPress={this.props.onPress} />
                </div>
                <div className="mt-3">
                    <p className='p-1'>{this.props.address}</p>
                    <h3>{this.props.balance} ETH</h3>
                </div>
            </div>
        </div>
      );
    }
}

export default AmountWallet;