import React from 'react';

import { faWarning } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface Props {
    days: number
}
  
class Button extends React.Component<Props> {

    getAlert() {
        if(this.props.days > 365) {
            return (
                <div className="alert alert-danger" role="alert">
                    <FontAwesomeIcon icon={faWarning} /> Wallet is old!
                </div>          
            )
        }
    }

    render() {
      return (
          <>{this.getAlert()}</>
      );
    }
  }

  export default Button;