import React from 'react';

import { faCheck, faClose } from '@fortawesome/free-solid-svg-icons';

import Button from "./Button";

interface Props {
    onChange: any
}  

class InputWallet extends React.Component<Props> {

    private input = React.createRef<HTMLInputElement>();

    state = {
        msg: false
    }

    async handleCheck (e:any) {
        e.preventDefault();

        if(this.input &&  this.input.current && (
            this.input.current.value === '' || this.input.current?.value === undefined)
        ) {
            this.setState({msg: true});
            return;
        } 
        
        const params = { 
            address: this.input?.current?.value
        };

        this.props.onChange(params);
    }

    handleClose(e:any) {
        e.preventDefault();

        this.setState({msg: false});
        this.props.onChange(false);
    }

    getMsg() {
        const { msg } = this.state
        if(msg) return (<p className='text-danger p-1'>Complete the field</p>);
    }

    componentDidMount() {
        if (this.input.current) {
            this.input.current.focus();
        }
    }

    render() {
      return (
        <div className="card bg-info bg-opacity-10">
            <div className="card-body min-vh-25">
                <div className="d-flex justify-content-end">
                    <Button icon={faClose} color="red" onPress={this.handleClose.bind(this)} />
                    <Button icon={faCheck} color="green" onPress={this.handleCheck.bind(this)} />
                </div>
                <div className="mt-3">
                    <input 
                        type="text" 
                        className="form-control" 
                        placeholder="Address ETH (ERC-20)"
                        name="address"
                        ref={this.input}
                    />
                    {this.getMsg()}
                </div>
            </div>
        </div>
      );
    }
}

export default InputWallet;