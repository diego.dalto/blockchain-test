import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IconProp } from '@fortawesome/fontawesome-svg-core'

interface Props {
    icon: IconProp,
    color: string,
    text?: string,
    onPress: any,
}
  
class Button extends React.Component<Props> {

    render() {
      return (
        <button onClick={this.props.onPress} className="btn link-light btn-sm">
            {(this.props.text) ? <span>{this.props.text}</span> : ""}
            <FontAwesomeIcon icon={this.props.icon} color={this.props.color} />
        </button>
      );
    }
  }

  export default Button;