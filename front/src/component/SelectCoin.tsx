import React from 'react';

import 'bootstrap/dist/css/bootstrap.css';
interface Props {
    price_usd: number,
    price_eur: number
}  

class SelectCoin extends React.Component<Props> {

    state = {
        coin: 'USD'
    };

    handleChange(event:any) {
        this.setState({coin: event.target.value});
    }

    getRate() {
        const { coin } = this.state;
        const { price_usd, price_eur } = this.props

        if(coin === 'USD') {
            return <h3>{price_usd} {coin}</h3>
        }

        return <h3>{price_eur} {coin}</h3>
    }

    render() {
      return (
        <div className="card bg-info bg-opacity-10">
            <div className="card-body min-vh-25">
                <div className="mt-5">
                    <select onChange={this.handleChange.bind(this)} className="form-select mb-2">
                        <option value="USD">US Dollar</option>
                        <option value="EUR">Euro</option>
                    </select>
                    {this.getRate()}
                </div>
            </div>
        </div>
      );
    }    
}

export default SelectCoin;