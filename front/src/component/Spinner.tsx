import React from 'react';

interface Props {
    show: boolean
}
  
class Spinner extends React.Component<Props> {

    getSpinner() {
        if(this.props.show) {
            return (
                <div className="spinner-grow" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            )
        }
    }

    render() {
      return (
          <>{this.getSpinner()}</>
      );
    }
  }

  export default Spinner;