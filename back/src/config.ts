import * as path from 'path';
import { Logger } from '@nestjs/common';

const logger = new Logger('Config');
const dotenv = require('dotenv');

dotenv.config({
  path: path.resolve(__dirname, '../../.env')
});

export class Config {
  'clientApp.name' = 'Digital Wallet Server';
  'swagger.default-include-pattern' = '/api/.*';
  'swagger.title' = 'Digital Wallet API';
  'swagger.description' = 'Digital Wallet API documentation';
  'swagger.version' = '1.0';
  'swagger.path' = '/api/v1/docs';
  'server.port' = '8080';
  'ETHERSCAN_APIKEY' = process.env.COINMARKETCAP_APIKEY || 'NSZCD6S4TKVWRS13PMQFMVTNP6H7NAGHUY';
  'MONGOOSE_URI' = process.env.MONGOOSE_URI || 'mongodb://admin:password@mongo:27017/dbase?authSource=admin';
  'MONGOOSE_SSL' = process.env.MONGOOSE_SSL || false;
  'MONGOOSE_SSL_CA' = process.env.MONGOOSE_SSL_CA || '';
  'REDIS_HOST' = process.env.REDIS_HOST || '127.0.0.1';
  'REDIS_PORT' = process.env.REDIS_PORT || 6379;

  constructor(properties) {
    this.addAll(properties);
  }

  public get(key: string): any {
    return this[key];
  }

  public is(key: string): Boolean {
    return (this[key] === "true");
  }

  public addAll(properties): any {
    properties = objectToArray(properties);
    for (const property in properties) {
      if (properties.hasOwnProperty(property)) {
        this[property] = properties[property];
      }
    }
    this.postProcess();
  }

  public postProcess(): any {
    const variables = { ...this, ...process.env };
    for (const property in this) {
      if (this.hasOwnProperty(property)) {
        const value = this[property];
        const processedValue = this.processTemplate(value, variables);
        this[property] = processedValue;
      }
    }
  }

  private processTemplate(template, variables): any {
    if (typeof template === 'string') {
      return template.replace(new RegExp('\\${[^{]+}', 'g'), name => variables[name.substring(2, name.length - 1)]);
    }
    return template;
  }
}

logger.log(`Actual process.env.NODE_ENV value: ${process.env.NODE_ENV}`);
logger.log('Standard allowed values are: dev, test or prod');

const config = new Config({ ipAddress: ipAddress() });

export { config };

function objectToArray(source, currentKey?, target?): any {
  target = target || {};
  for (const property in source) {
    if (source.hasOwnProperty(property)) {
      const newKey = currentKey ? currentKey + '.' + property : property;
      const newVal = source[property];

      if (typeof newVal === 'object') {
        objectToArray(newVal, newKey, target);
      } else {
        target[newKey] = newVal;
      }
    }
  }
  return target;
}

function ipAddress(): any {
  const interfaces = require('os').networkInterfaces();
  for (const dev in interfaces) {
    if (interfaces.hasOwnProperty(dev)) {
      const iface = interfaces[dev];
      for (const alias of iface) {
        if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
          return alias.address;
        }
      }
    }
  }

  return null;
}
