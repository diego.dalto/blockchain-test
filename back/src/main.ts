import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { setupSwagger } from './swagger';

import { config } from './config';

async function bootstrap() {
  const appOptions = { cors: true };

  const app = await NestFactory.create(AppModule, appOptions);
  app.enableCors();

  setupSwagger(app);

  await app.listen(config.get('server.port'));
}

bootstrap();