export class ResponsePriceConversion {
    status: {
        timestamp: string,
        error_code: number,
        error_message: any,
        elapsed: number,
        credit_count: number,
        notice: any
    };
    data: {
        id: number,
        symbol: string,
        name: string,
        amount: number,
        last_updated: string,
        quote: [
            {
                price: number,
                last_updated: string
            }
        ]
    }
}