import { Injectable, Logger } from '@nestjs/common';
import axios, { AxiosInstance } from 'axios';

import { ResponsePriceConversion } from './dto/ResponsePriceConversion';

const API_URL = 'https://web-api.coinmarketcap.com';
const VERSION = 'v1';

@Injectable()
export class CoinMarketCapService {
  private logger = new Logger(CoinMarketCapService.name);
  private apiClient: AxiosInstance;

  constructor() {
    this.init();
  }

  private init() {
    this.apiClient = axios.create({
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Accept-Encoding': 'deflate, gzip'
        },
        responseType: 'json',
        baseURL: `${API_URL}/${VERSION}`
    });
  }

  async converToFiat(symbol: string) {
    let response:ResponsePriceConversion;
    try {
      response = (
        await this.apiClient.get<ResponsePriceConversion>('/tools/price-conversion', {
            params: {
                amount: 1,
                symbol: symbol,
                convert_id: '2790,2781'
            }
        })
      )?.data;
    } catch (err) {
        this.logger.error(`Error: ${err}`);

        response = new ResponsePriceConversion();
        response.status.error_code = 0;
        response.status.error_message = "Catch error";
    }
    return response;
  }
}