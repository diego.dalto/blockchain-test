import { CacheModule, Module } from '@nestjs/common';

import * as redisStore from 'cache-manager-redis-store';

import { CoinMarketCapService } from './coinmarketcap.service';
import { CoinMarketCapManager } from './coinmarketcap.manager';

import { config } from '../config';

@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: config.get('REDIS_HOST'),
      port: config.get('REDIS_PORT')
    }),
  ],
  providers: [
    CoinMarketCapService,
    CoinMarketCapManager
  ],
  exports: [
    CoinMarketCapManager
  ]
})
export class CoinMarketCapModule {}
