import { 
    CACHE_MANAGER, 
    Inject, 
    Injectable, 
    Logger 
} from '@nestjs/common';
import { Cache } from 'cache-manager';

import * as _ from 'lodash';

import { CoinMarketCapService } from './coinmarketcap.service';
import { ResponsePriceConversion } from './dto/ResponsePriceConversion';

@Injectable()
export class CoinMarketCapManager {
  logger = new Logger(CoinMarketCapManager.name);

  private fiats: Map<String, Number>;

  constructor(
    @Inject(CACHE_MANAGER) protected readonly cacheManager: Cache, 
    private readonly coinmarketCap: CoinMarketCapService
  ) {    
    this.fiats = new Map<String, Number>();
    this.fiats.set('EUR', 2790);
    this.fiats.set('USD', 2781);
  }


  async getFiatPrice(base: string, quote: string): Promise<number> {
    this.logger.verbose(`getFiatPrice from base: ${base} and quote: ${quote}`);

    const key = `FIATPRICE::VALUE::${base}`;
    const quote_id = this.fiats.get(quote);

    let converToFiat = await this.cacheManager.get<ResponsePriceConversion>(key);

    if (!converToFiat) {
      converToFiat = await this.coinmarketCap.converToFiat(base);

      await this.cacheManager.set<ResponsePriceConversion>(key, converToFiat, { ttl: 43200 });
    }

    return converToFiat?.data?.quote[<number>quote_id]?.price || 0;
  }
}