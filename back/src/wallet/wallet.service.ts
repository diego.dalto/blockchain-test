import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import * as _ from 'lodash';
import * as moment from 'moment';

import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { ResponseWallet } from './dto/ResponseWallet.dto';

import { Wallet, WalletDocument } from './entities/wallet.entity';

import { CoinMarketCapManager } from '../coinmarketcap/coinmarketcap.manager';
import { EtherscanService } from '../etherscan/etherscan.service';

@Injectable()
export class WalletService {
  logger = new Logger(WalletService.name);

  constructor(
    private readonly etherscanService: EtherscanService,
    private readonly coinMarketCapManager: CoinMarketCapManager,
    @InjectModel(Wallet.name) private walletModel: Model<WalletDocument>
  ) { }
  
  async create(createWalletDto: CreateWalletDto): Promise<ResponseWallet> {
    const wallet = new Wallet();
    wallet.address = createWalletDto.address;
    
    const responseWallet = new ResponseWallet();

    const balance = await this.etherscanService.getAccountBalance(createWalletDto.address);
    // Balance result in wei to eth
    if(balance.hasOwnProperty('status') && balance.status == 1) {
      const eth = balance.result / (10 ** 18);
      wallet.balance = eth;

      wallet.price_eur = await this.getPrice(eth, 'EUR');
      wallet.price_usd = await this.getPrice(eth, 'USD');
    }

    const accountTXList = await this.etherscanService.getAccountTXList(createWalletDto.address);
    // Transactions result
    if(accountTXList.hasOwnProperty('status') && accountTXList.status == 1) {
      const first = _.head(accountTXList.result);
      const start = moment.unix(first.timeStamp);
      const end = moment();

      wallet.days = end.diff(start, 'days');
    }

    try {
      await this.walletModel.insertMany(wallet);
      responseWallet.assign(wallet);
      responseWallet.status = 1;
    } catch (e) {
      this.logger.error(`Error: ${e}`);      
    }

    return responseWallet;
  }

  async findAll() {
    return await this.walletModel.find();
  }

  async findOne(id: number) {
    return await this.walletModel.findOne();
  }

  async update(id: string, updateWalletDto: UpdateWalletDto) {
    return await this.walletModel.updateOne({_id: id}, {$set: updateWalletDto});
  }

  private async getPrice(balance: number, quote: string): Promise<number> {
    const price = await this.coinMarketCapManager.getFiatPrice('ETH', quote);

    if(price)
      return balance * price;

    return 0;
  }
}