import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type WalletDocument = Wallet & Document;

@Schema({timestamps: true})
export class Wallet {

    @Prop()
    address: string;

    @Prop()
    balance: number;

    @Prop({ default: false })
    favourite: boolean;

    @Prop({ default: 0 })
    days: number;

    @Prop()
    price_usd: number;

    @Prop()
    price_eur: number;
}

export const WalletSchema = SchemaFactory.createForClass(Wallet);