import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateWalletDto {
    @ApiProperty({ description: 'address', required: true })
    @IsString()
    address: string;
}
