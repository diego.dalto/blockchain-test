import { Wallet } from '../entities/wallet.entity';

export class ResponseWallet {
    address: string;
    balance: number;
    favourite: boolean;
    days: number;
    price_usd: number;
    price_eur: number;
    status: number = 0;

    assign(wallet: Wallet) {
        Object.assign(this, wallet);
    }
};
 