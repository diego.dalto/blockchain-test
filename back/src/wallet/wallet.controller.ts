import { Controller, Get, Post, Body, Param,  Put } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { WalletService } from './wallet.service';

import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { ResponseWallet } from './dto/ResponseWallet.dto';


@Controller('api/wallet')
@ApiTags('Wallet')
export class WalletController {
  constructor(private readonly walletService: WalletService) {}

  @ApiResponse({
    status: 201,
    description: 'The wallet has been successfully created.',
  })
  @Post('/')
  async create(@Body() createWalletDto: CreateWalletDto): Promise<ResponseWallet> {    
    return await this.walletService.create(createWalletDto);
  }

  @ApiResponse({
    status: 200,
    description: 'List of wallets',
  })
  @Get('/')
  async findAll() {
    return await this.walletService.findAll();
  }

  @ApiResponse({
    status: 200,
    description: 'Specific wallet id',
  })
  @Get('/:id')
  async findOne(@Param('id') id: string) {
    return await this.walletService.findOne(+id);
  }

  @Put('/:id')
  async update(@Param('id') id: string, @Body() updateWalletDto: UpdateWalletDto) {
    return await this.walletService.update(id, updateWalletDto);
  }
}
