import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { WalletService } from './wallet.service';
import { WalletController } from './wallet.controller';
import { Wallet, WalletSchema } from './entities/wallet.entity';

import { CoinMarketCapModule } from '../coinmarketcap/coinmarketcap.module';
import { EtherscanModule } from '../etherscan/etherscan.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ 
      name: Wallet.name, schema: WalletSchema 
    }]),
    EtherscanModule,
    CoinMarketCapModule
  ],
  controllers: [WalletController],
  providers: [WalletService]
})
export class WalletModule {}
