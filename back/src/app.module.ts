import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WalletModule } from './wallet/wallet.module';

import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';

import { config } from './config';

const options: MongooseModuleOptions = {
  ssl: config.get('MONGOOSE_SSL'),
  tlsCertificateKeyFile: config.get('SSL_CA')
}
@Module({
  imports: [
    WalletModule,
    MongooseModule.forRoot(config.MONGOOSE_URI, options)
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
