export class ResponseAccountTXList {
   status: number;
   message: string;
   result: Array<Transactions>;
}

export class ResponseAccountBalance {
   status: number;
   message: string;
   result: number;
}

export class Transactions {
   blockNumber: number;
   timeStamp: number;
   hash: string;
   nonce?: string;
   blockHash?: string;
   transactionIndex: number;
   from: string;
   to: string;
   value: number;
   gas: number;
   gasPrice: number;
   isError:number;
   txreceipt_status?: string;
   input?: string;
   contractAddress?: string;
   cumulativeGasUsed:number;
   gasUsed:number;
   confirmations:number;
};
