import { Injectable, Logger } from '@nestjs/common';
import axios, { AxiosInstance } from 'axios';

import { 
  ResponseAccountBalance,
  ResponseAccountTXList
} from './dto/ResponseAccount.dto';

import { config } from '../config';

const API_URL = 'https://api.etherscan.io/api';

@Injectable()
export class EtherscanService {
  private logger = new Logger(EtherscanService.name);
  private apiKey: string;
  private apiClient: AxiosInstance;

  constructor() {
    this.apiKey = config.get("ETHERSCAN_APIKEY");
    this.init();
  }

  private init() {
    this.apiClient = axios.create({
      headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Accept-Encoding': 'deflate, gzip'
      },
      responseType: 'json',
      baseURL: `${API_URL}`
    });
  }

  async getAccountTXList(address: string): Promise<ResponseAccountTXList> {
    let response:ResponseAccountTXList;

    try {
      response = (
        await this.apiClient.get<ResponseAccountTXList>('/', {
          params: {
              module: "account",
              action: "txlist",
              address: address,
              page: 1,
              offset: 2,
              sort: "asc",
              apikey: this.apiKey
          }
        })
      )?.data;
    } catch (err) {
      this.logger.error(`Error: ${err}`);

      response = new ResponseAccountTXList();
      response.status = 0;
      response.message = "Catch error";
    }

    return response;
  }

  async getAccountBalance(address: string) : Promise<ResponseAccountBalance> {
    let response:ResponseAccountBalance;

    try {
      response = (
        await this.apiClient.get<ResponseAccountBalance>('/', {
          params: {
              module: "account",
              action: "balance",
              address: address,
              tag: "latest",
              apikey: this.apiKey
          }
        })
      )?.data;
      
    } catch (err) {
      this.logger.error(`Error: ${err}`);

      response = new ResponseAccountBalance();
      response.status = 0;
      response.message = "Catch error";
    }

    return response;
  }
}